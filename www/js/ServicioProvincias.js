/**
 * Servicio para recuperar las provincias que hay en el servidor rest
 */
angular.module("starter.servicioProvincias", [])
    .factory('ServicioProvincias', function ($resource) {

        var datos = [];
        //datos = $resource('http://localhost:1337/antonioramirez.esy.es/acontecimientos/.', {}, {
            datos = $resource('http://antonioramirez.esy.es/acontecimientos/provincias/.', {}, {
                get: {
                    method: 'GET',
                    isArray: true/*,
                    transformResponse: function(data) {
                        return angular.fromJson(data);
                    }*/
                }
            });
        
        return datos;
    });