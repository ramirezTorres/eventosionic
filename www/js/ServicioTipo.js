/**
 * Servicio para obtener los tipos de acontecimientos que hay almacenado en el servidor rest, dependiendo de la provincia
 */
angular.module("starter.servicioTipo", [])
    .factory('ServicioTipo', function ($resource) {

        var datos = [];
            datos = $resource('http://antonioramirez.esy.es/acontecimientos/tipos/:prov', {prov: "@prov"}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        
        return datos;
    });