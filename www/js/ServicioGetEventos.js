/**
 * Servicio para obtener la lista de eventos de un acontecimiento, según el id del acontecimiento
 */
angular.module("starter.servicioGetEventos", [])
    .factory('ServicioGetEventos', function ($resource) {
       var datos = [];
       datos = $resource('http://antonioramirez.esy.es/acontecimientos/cogeEventos/:id', {id: "@id"}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        
        return datos;
    });