/**
 * Controlador del mapa, primero ubica el mapa en la localización del acontecimiento que le mandamos, con el botón inferior se 
 * hace la geolocalización
 */
angular.module('starter')

    .controller('MapaController', function ($ionicPlatform, $cordovaGeolocation, $state, $scope, /*$http,*/ $rootScope, $ionicLoading/*, ServicioProvincias, ServicioTipo, ServicioBuscarAcontecimientos*/) {

        //carga del mapa del acontecimiento
        function initialize() {
            //obtengo la latitud y la longitud
            var latLng = new google.maps.LatLng($rootScope.latLong.longitud, $rootScope.latLong.latitud);

            //opciones del mapa
            var mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            //alert($rootScope.latLong.Nombre);

            //creo el mapa
            $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

            //google.maps.event.addListenerOnce($scope.map, 'idle', function () {
            
            //marcador para el mapa
            var marcadorActual = new google.maps.Marker({
                map: $scope.map,//mapa a donde va el marcador
                position: latLng,//latitud y longitud
                title: $rootScope.latLong.nombre//titulo con el nombre del acontecimiento
            });

            //var nombreAcontecimiento = $rootScope.infMap.Nombre;

            //carga la información del mapa
            var infoWindow = new google.maps.InfoWindow({
                content: marcadorActual.title
            });

            //listener, si pulsamos sobre el marcador mostramos la información del marcador
            google.maps.event.addListener(marcadorActual, 'click', function () {
                infoWindow.open($scope.map, marcadorActual);
            });

            // });
        };

        if (document.readyState === "complete") {
            initialize();//inicia el mapa
        } else {
            google.maps.event.addDomListener(window, 'load', initialize);
        }
        /*
                document.addEventListener("deviceready", onDeviceReady, false);
                function onDeviceReady() {
                    console.log("navigator.geolocation works well");*/


        //función para la localización
        $scope.localizame = function () {
            alert("entrando en localizame");

            /*var myLatlng = new google.maps.LatLng(37.3000, -120.4833);

            var mapOptions = {
                center: myLatlng,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("map"), mapOptions);*/

            var posOptions = { timeout: 10000, enableHighAccuracy: false };
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {
                    var lat = position.coords.latitude
                    var long = position.coords.longitude
                    alert(lat + "," + long);
                    $scope.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                    var marcadorActual = new google.maps.Marker({
                        map: $scope.map,
                        position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                        title: "Tú estás aquí!!"
                    });
                    var infoWindow = new google.maps.InfoWindow({
                        content: marcadorActual.title
                    });

                    google.maps.event.addListener(marcadorActual, 'click', function () {
                        infoWindow.open($scope.map, marcadorActual);
                    });
                    //$scope.loading.hide();
                }, function (err) {
                    alert('Unable to get location: ' + error.message);
                    // error
                });
                
                

            /*navigator.geolocation.getCurrentPosition(function success(pos) {

                map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));

                alert("dentro del getcurrentposition");
                var myLocation = new google.maps.Marker({
                    position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
                    map: map,
                    title: "My Location"
                });

                $scope.map = map;

            }, function error(error) {
                alert('Unable to get location: ' + error.message);
            });*/

        };
        // };

    });
