/**
 * Controlador para la vista de inicio, crea la base de datos y sus tablas si no existen
 * Posteriormente obtiene los datos de la base de datos para mostrar los acontecimientos en la vista
 */
var db = null;
angular.module('starter')

    .controller('InicioController', /*['$scope', '$http', '$cordovaSQLite',*/ function ($ionicPlatform, $scope, $http, $cordovaSQLite, $rootScope) {

        $ionicPlatform.ready(function () {
            //$cordovaSQLite.deleteDB("datos.db");//por si quiero borrar la bd
            db = $cordovaSQLite.openDB("datos.db");//abro la bd

            //sentencia que crea la tabla Acontecimientos si no existe
            $cordovaSQLite.execute(db,
                'CREATE TABLE IF NOT EXISTS Acontecimientos(' +
                'Id INTEGER PRIMARY KEY, ' +
                'Nombre TEXT, ' +
                'Organizador TEXT, ' +
                'Descripcion TEXT, ' +
                'Tipo TEXT, ' +
                'Telefono TEXT, ' +
                'Email TEXT, ' +
                'Provincia TEXT, ' +
                'Poblacion TEXT, ' +
                'Codigo_Postal INTEGER, ' +
                'Direccion TEXT, ' +
                'Web TEXT, ' +
                'Facebook TEXT, ' +
                'Twitter TEXT, ' +
                'Inicio DATETIME, ' +
                'Fin DATETIME, ' +
                'Ubicacion TEXT, ' +
                'Longitud NUMERIC, ' +
                'Latitud NUMERIC)');

            //sentencia que crea la tabla Eventos si no existe
            $cordovaSQLite.execute(db,
                'CREATE TABLE IF NOT EXISTS Eventos(' +
                'Id_Evento INTEGER PRIMARY KEY, ' +
                'Id_Acontecimiento INTEGER, ' +
                'Nombre_Evento TEXT, ' +
                'Persona TEXT, ' +
                'Descripcion TEXT, ' +
                'Telefono TEXT, ' +
                'Email TEXT, ' +
                'Provincia TEXT, ' +
                'Poblacion TEXT, ' +
                'Direccion TEXT, ' +
                'Codigo_Postal INTEGER, ' +
                'Web TEXT, ' +
                'Facebook TEXT, ' +
                'Twitter TEXT, ' +
                'Inicio DATETIME, ' +
                'Fin DATETIME, ' +
                'Ubicacion TEXT, ' +
                'Longitud NUMERIC, ' +
                'Latitud NUMERIC, ' +
                'FOREIGN KEY(Id_Acontecimiento) REFERENCES Acontecimientos(Id))');

            //obtengo los acontecimientos
            var query = "SELECT * FROM Acontecimientos";
            $cordovaSQLite.execute(db, query, []).then(function (res) {
                if (res.rows.length > 0) {
                    $scope.listaAcontecimientosLocal = [];//lista donde voy a guardar los acontecimientos obtenidos
                    //alert("SELECTED -> " + res.rows.item(0).Id + " " + res.rows.item(0).Nombre);
                    for (var i = 0; i < res.rows.length; i++) {
                        var aux = {//campos que voy a recuperar
                            Id: res.rows.item(i).Id,//id del acontecimiento
                            Nombre: res.rows.item(i).Nombre,//nombre del acontecimiento
                            Organizador: res.rows.item(i).Organizador,//organizador del acontecimiento
                            Tipo: res.rows.item(i).Tipo,//tipo de acontecimiento
                            Ubicacion: res.rows.item(i).Ubicacion//ubicación del acontecimiento
                        };

                        $scope.listaAcontecimientosLocal.push(aux);//lo inserto en la lista
                    }


                } else {
                    alert("Ningún resultado");
                    $scope.listaAcontecimientosLocal.push("Ningún resultado");
                }
            }, function (err) {
                console.error(err);
                alert(err);
            });
        });
        
        //le asigno a $rootScope el id seleccionado para pasarlo entre vistas
        $scope.setAcontecimiento = function (idSeleccionado) {
            $rootScope.IDAcontecimientoSeleccionado = idSeleccionado;  
            //alert("Seleccionado " + $rootScope.IDAcontecimientoSeleccionado);
        };
    });