/**
 * Controlador para buscar en el servidor rest los datos de un acontecimiento
 */
angular.module('starter')
  
    .controller('BuscarController', function ($scope, $http, $rootScope, ServicioProvincias, ServicioTipo, ServicioBuscarAcontecimientos) {
            $scope.Provincias = ServicioProvincias.get();//obtengo las provincias
            console.log($scope.Provincias);
            
            $scope.buscarTipo = function(provincia){//obtengo el tipo de acontecimiento dependiendo de la provincia seleccionada
                $scope.Tipo = ServicioTipo.get({prov: provincia});
            };
            
            //obtengo el acontecimiento pasando la provincia, el tipo y la fecha
            $scope.buscarAcontecimientos = function (provincia, tipoSeleccionado, fechaSeleccionado) {
                $rootScope.resultadoBusquedaAcontecimientos = ServicioBuscarAcontecimientos.get({prov: provincia, tipo: tipoSeleccionado, fecha: fechaSeleccionado});
            };
            
            
    });