/**
 * Servicio que obtiene del servidor rest un ÚNICO acontecimiento, por su ID
 */
angular.module("starter.servicioGetAcontecimiento", [])
    .factory('ServicioGetAcontecimiento', function ($resource) {
       var datos = {};
       datos = $resource('http://antonioramirez.esy.es/acontecimientos/cogeAcontecimiento/:id', {id: "@id"}, {
                get: {
                    method: 'GET',
                    isArray: false
                }
            });
        
        return datos;
    });