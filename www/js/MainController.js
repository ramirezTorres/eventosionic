angular.module('starter')
//configuracion de los estados para movernos entre las distintas vistas
    .config(function ($stateProvider, $urlRouterProvider) {
		

        //estado inicio
        $stateProvider.state('inicio', {
            cache: false, //borro la cache para que se refresquen los datos
            url: '/inicio', //url
            views: {
                'contenido': {
                    templateUrl: 'views/inicio.html', //plantilla usada
                    controller: 'InicioController', //controlador usado
                    controllerAs: 'inicio' //alias del controlador
                }
            }
        });
		
        //estado buscar
        $stateProvider.state('buscar', {
            cache: false, //borro la cache para que se refresquen los datos
            url: '/buscar', //url
            views: {
                'contenido': {
                    templateUrl: 'views/buscar.html', //plantilla usada
                    controller: 'BuscarController', //controlador usado
                    controllerAs: 'buscar' //alias del controlador
                }
            }
        });
        
        //estado listado_acontecimientos_buscados
        $stateProvider.state('listar_acontecimientos_buscados', {
            cache: false, //borro la cache para que se refresquen los datos
            url: '/listar_acontecimientos_buscados', //url
            views: {
                'contenido': {
                    templateUrl: 'views/listar_acontecimientos_buscados.html', //plantilla usada
                    controller: 'ListaACBuscadosController', //controlador usado
                    controllerAs: 'ListaAcBuscadosCtrl' //alias del controlador
                }
            }
        });
        
        //estado camara
        $stateProvider.state('camara', {
            cache: false, //borro la cache para que se refresquen los datos
            url: '/camara', //url
            views: {
                'contenido': {
                    templateUrl: 'views/camara.html', //plantilla usada
                    controller: 'CamaraController', //controlador usado
                }
            }
        });
        
        //estado acontecimientoSeleccionado
        $stateProvider.state('acontecimientoSeleccionado', {
            cache: false, //borro la cache para que se refresquen los datos
            url: "/acontecimientoSeleccionado", //url
            views: {
                'contenido': {
                    templateUrl: 'views/acontecimientoSeleccionado.html', //plantilla usada
                    controller: 'AcontecimientoSeleccionadoController', //controlador usado
                    controllerAs: 'CTRLacontecimiento' //alias del controlador
                }
            }
        });
        
        //estado mapa
        $stateProvider.state('mapa', {
            cache: false, //borro la cache para que se refresquen los datos
            url: "/mapa", //url
            views: {
                'contenido': {
                    templateUrl: 'views/mapa.html', //plantilla usada
                    controller: 'MapaController' //controlador usado
                }
            }
        });

        $urlRouterProvider.otherwise('/inicio');

    })

    .controller('MainController', /*['$scope', '$http', '$rootScope',*/ function ($scope, $http, $rootScope) {
        //variables globales
        $scope.Provincias = [];
        $scope.Tipo = [];
        $rootScope.resultadoBusquedaAcontecimientos = [];
        $rootScope.IDAcontecimientoSeleccionado = 0;

        //para la publicidad
        var admobid = {};
        if (/(android)/i.test(navigator.userAgent)) {
            admobid = { // for Android
                banner: 'ca-app-pub-3541256200483341/9403938113',
                interstitial: 'ca-app-pub-3541256200483341/7927204915'
            };
        } else if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
            admobid = { // for iOS
                banner: 'ca-app-pub-3541256200483341/9403938113',
                interstitial: 'ca-app-pub-3541256200483341/7927204915'
            };
        } else {
            admobid = { // for Windows Phone
                banner: 'ca-app-pub-3541256200483341/9403938113',
                interstitial: 'ca-app-pub-3541256200483341/7927204915'
            };
        }

        if ((/(ipad|iphone|ipod|android|windows phone)/i.test(navigator.userAgent))) {
            document.addEventListener('deviceready', initApp, false);
        } else {
            initApp();
        }

        function initApp() {
            if (!AdMob) { alert('admob plugin not ready'); return; }

            AdMob.createBanner({
                adId: admobid.banner,
                isTesting: true,
                overlap: false,
                offsetTopBar: false,
                position: AdMob.AD_POSITION.BOTTOM_CENTER,
                bgColor: 'black'
            });

            AdMob.prepareInterstitial({
                adId: admobid.interstitial,
                autoShow: true
            });
        }
    }/*]*/);