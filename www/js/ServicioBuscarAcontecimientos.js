/**
 * Servicio para obtener la lista de acontecimientos del servidor rest
 */
angular.module("starter.servicioBuscarAcontecimientos", [])
    .factory('ServicioBuscarAcontecimientos', function ($resource) {

        var datos = [];
            datos = $resource('http://antonioramirez.esy.es/acontecimientos/buscarAcontecimientos/:prov/:tipo/:fecha', {prov: "@prov", tipo: "@tipo", fecha: "@fecha"}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        
        return datos;
    });