// Ionic Starter App
var db = null;
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic',
    'ngCordova',
    'ngResource',
    'starter.servicioProvincias',
    'starter.servicioTipo',
    "starter.servicioBuscarAcontecimientos",
    "starter.servicioGetAcontecimiento",
    "starter.servicioGetEventos",
    "DirectivaMapa"
])

    .run(function ($ionicPlatform, $cordovaSQLite, $rootScope) {
        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
    
            //$cordovaSQLite.deleteDB("datos.db");

            //db = $cordovaSQLite.openDB("datos.db");
            //$rootScope.db = db;

            /*
            $cordovaSQLite.execute(db,
                'CREATE TABLE IF NOT EXISTS Acontecimientos(' +
                'Id INTEGER PRIMARY KEY, ' +
                'Nombre TEXT, ' +
                'Organizador TEXT, ' +
                'Descripcion TEXT, ' +
                'Tipo TEXT, ' +
                'Telefono TEXT, ' +
                'Email TEXT, ' +
                'Provincia TEXT, ' +
                'Poblacion TEXT, ' +
                'Codigo_Postal INTEGER, ' +
                'Direccion TEXT, ' +
                'Web TEXT, ' +
                'Facebook TEXT, ' +
                'Twitter TEXT, ' +
                'Inicio DATETIME, ' +
                'Fin DATETIME, ' +
                'Ubicacion TEXT, ' +
                'Longitud NUMERIC, ' +
                'Latitud NUMERIC)');
                
            $cordovaSQLite.execute(db,
                'CREATE TABLE IF NOT EXISTS Eventos(' +
                'Id_Evento INTEGER PRIMARY KEY, ' +
                'Id_Acontecimiento INTEGER, ' +
                'Nombre_Evento TEXT, ' +
                'Persona TEXT, ' + 
                'Descripcion TEXT, ' +
                'Telefono TEXT, ' +
                'Email TEXT, ' +
                'Provincia TEXT, ' +
                'Poblacion TEXT, ' +
                'Direccion TEXT, ' +
                'Codigo_Postal INTEGER, ' +
                'Web TEXT, ' +
                'Facebook TEXT, ' +
                'Twitter TEXT, ' +
                'Inicio DATETIME, ' +
                'Fin DATETIME, ' +
                'Ubicacion TEXT, ' +
                'Longitud NUMERIC, ' +
                'Latitud NUMERIC, ' + 
                'FOREIGN KEY(Id_Acontecimiento) REFERENCES Acontecimientos(Id))');
                /*
                $cordovaSQLite.execute(db,
                'CREATE TABLE IF NOT EXISTS Acontecimientos(' +
                'Id INTEGER PRIMARY KEY, ' +
                'Nombre TEXT)');
                
                $cordovaSQLite.execute(db, 
                'insert into Acontecimientos (Id, Nombre) ' +
                'values (10, "Acontecimiento1")');*/
            /*
            var query = "SELECT * FROM Acontecimientos";
            $cordovaSQLite.execute(db, query, []).then(function(res) {
                if(res.rows.length > 0) {
                    alert("SELECTED -> " + res.rows.item(0).Id + " " + res.rows.item(0).Nombre);    
                } else {
                    alert("Ningún resultado");
                }
            }, function (err) {
                console.error(err);
            });*/
        });

        /** Para las notificaciones de Onesignal */
        document.addEventListener('deviceready', function () {
            // Enable to debug issues.
            // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
  
            var notificationOpenedCallback = function (jsonData) {
                console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
            };

            //datos de para Onesignal
            window.plugins.OneSignal.init("f8ac1ef9-08fc-4ce6-80a1-5ceeb18cc5ab",
                { googleProjectNumber: "619860583283" },
                notificationOpenedCallback);
  
            // Show an alert box if a notification comes in when the user is in your app.
            window.plugins.OneSignal.enableInAppAlertNotification(true);
        }, false);
    })/*
.controller('ControladorBD', function ($scope, $rootScope, $cordovaSQLite/*, ServicioProvincias, ServicioTipo, ServicioBuscarAcontecimientos) {
         /*   
            $scope.AsignaBD = function () {
                $rootScope.db = db;
                alert($rootScope.db);
            };
            
    });*/
