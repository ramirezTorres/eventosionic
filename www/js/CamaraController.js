/**
 * Controlador para la cámara de fotos
 */
angular.module('starter')

    .controller('CamaraController', function ($ionicPlatform, $cordovaCamera, $cordovaFile, $scope, $http, $rootScope, ServicioProvincias, ServicioTipo, ServicioBuscarAcontecimientos) {
        $ionicPlatform.ready(function () {//opciones de la camara
            var options = {
                quality: 100,//calidad de la foto
                destinationType: Camera.DestinationType.FILE_URI,//destino
                sourceType: Camera.PictureSourceType.CAMERA,//fuente
                allowEdit: false,//por si tenemos aplicación para editar la foto y la queremos usar
                encodingType: Camera.EncodingType.JPEG,//tipo
                targetWidth: 100,
                targetHeight: 100,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: true//salvar la foto en el album
            };

            //función para hacer la foto
            $scope.takePicture = function () {
                $cordovaCamera.getPicture(options).then(function (imageData) {
                    $scope.imgSrc = "data:image/jpeg;base64," + imageData;
                    
                    //limpiamos el nombre de la foto
                    var currentName = imageData.replace(/^.*[\\\/]/, '');
 
                    //se crea un nuevo nombre con fecha y hora
                    var d = new Date(),
                        n = d.getTime(),
                        newFileName = n + ".jpg";
 
                    //una vez tomada la foto la movemos a una carpeta para dejarla almacenada
                    $cordovaFile.moveFile(cordova.file.tempDirectory, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
 
                    }, function (error) {
                        alert("Error al hacer la foto");
                    });
                }, function (err) {
                    console.log(err);
                });
            };



        });


    });