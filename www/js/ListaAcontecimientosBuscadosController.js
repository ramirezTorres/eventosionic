/**
 * Listamos los acontecimientos, y usamos este controlador cuando pulsamos sobre uno de ellos y lo almacenamos en la base de datos
 */
angular.module('starter')

    .controller('ListaACBuscadosController', function ($ionicPlatform, $scope, $http, $rootScope, $cordovaSQLite, ServicioGetAcontecimiento, ServicioGetEventos) {
        $scope.lista = $rootScope.resultadoBusquedaAcontecimientos;

        //añado el acontecimiento al la base de datos
        $scope.addAcontecimientoDB = function () {
            $ionicPlatform.ready(function () {
                $scope.eventosAcontecimiento = [];//declaración de los eventos del acontecimiento
                //obtenemos el acontecimiento y lo insertamos en la base de datos
                $scope.acontecimiento = ServicioGetAcontecimiento.get({ id: $rootScope.resultadoBusquedaAcontecimientos[0].Id }, function () {
                    $cordovaSQLite.execute(db,
                        'insert into Acontecimientos (Id, Nombre, Organizador, Descripcion, Tipo, ' +
                        'Telefono, Email, Provincia, Poblacion, Codigo_Postal, Direccion, Web, ' +
                        'Facebook, Twitter, Inicio, Fin, Ubicacion, Longitud, Latitud) ' +
                        'values (' + $scope.acontecimiento.Id + ', ' +
                        '"' + $scope.acontecimiento.Nombre + '", ' +
                        '"' + $scope.acontecimiento.Organizador + '", ' +
                        '"' + $scope.acontecimiento.Descripcion + '", ' +
                        '"' + $scope.acontecimiento.Tipo + '", ' +
                        '"' + $scope.acontecimiento.Telefono + '", ' +
                        '"' + $scope.acontecimiento.Email + '", ' +
                        '"' + $scope.acontecimiento.Provincia + '", ' +
                        '"' + $scope.acontecimiento.Poblacion + '", ' +
                        '' + $scope.acontecimiento.Codigo_Postal + ', ' +
                        '"' + $scope.acontecimiento.Direccion + '", ' +
                        '"' + $scope.acontecimiento.Web + '", ' +
                        '"' + $scope.acontecimiento.Facebook + '", ' +
                        '"' + $scope.acontecimiento.Twitter + '", ' +
                        '"' + $scope.acontecimiento.Inicio + '", ' +
                        '"' + $scope.acontecimiento.Fin + '", ' +
                        '"' + $scope.acontecimiento.Ubicacion + '", ' +
                        '' + $scope.acontecimiento.Longitud + ', ' +
                        '' + $scope.acontecimiento.Latitud + ')'
                        );
                });
                //obtenemos los eventos y los insertamos en la base de datos
                $scope.eventosAcontecimiento = ServicioGetEventos.get({ id: $rootScope.resultadoBusquedaAcontecimientos[0].Id }, function () {
                    alert($scope.acontecimiento.Organizador);
                    for (var i = 0; i < $scope.eventosAcontecimiento.length; i++) {//es una lista, hay que recorrer todos los elementos
                        $cordovaSQLite.execute(db,
                            'insert into Eventos (Id_Evento, Id_Acontecimiento, Nombre_Evento, Persona, Descripcion, ' +
                            'Telefono, Email, Provincia, Poblacion, Direccion, Codigo_Postal, Web, ' +
                            'Facebook, Twitter, Inicio, Fin, Ubicacion, Longitud, Latitud) ' +
                            'values (' + $scope.eventosAcontecimiento[i].Id_Evento + ', ' +
                            '' + $scope.eventosAcontecimiento[i].Id_Acontecimiento + ', ' +
                            '"' + $scope.eventosAcontecimiento[i].Nombre_Evento + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Persona + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Descripcion + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Telefono + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Email + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Provincia + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Poblacion + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Direccion + '", ' +
                            '' + $scope.eventosAcontecimiento[i].Codigo_Postal + ', ' +
                            '"' + $scope.eventosAcontecimiento[i].Web + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Facebook + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Twitter + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Inicio + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Fin + '", ' +
                            '"' + $scope.eventosAcontecimiento[i].Ubicacion + '", ' +
                            '' + $scope.eventosAcontecimiento[i].Longitud + ', ' +
                            '' + $scope.eventosAcontecimiento[i].Latitud + ')'
                            );
                            alert($scope.eventosAcontecimiento[i].Nombre_Evento);
                    }
                });
            });
        };
    });