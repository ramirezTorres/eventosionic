/**
 * Controlador para el acontecimiento seleccionado, obtenemos el acontecimiento y sus eventos de la base de datos sqlite
 */
var db = null;
angular.module('starter')

    .controller('AcontecimientoSeleccionadoController', /*['$scope', '$http', '$cordovaSQLite',*/ function ($ionicPlatform, $scope, $http, $stateParams, $cordovaSQLite, $rootScope) {

        $ionicPlatform.ready(function () {
            db = $cordovaSQLite.openDB("datos.db");//abrir base de datos

            var query = "SELECT * FROM Acontecimientos WHERE Id=?";//consulta para acontecimientos
            //ejecutamos la consulta
            $cordovaSQLite.execute(db, query, [$rootScope.IDAcontecimientoSeleccionado]).then(function (res) {
                if (res.rows.length > 0) {
                    //obtengo los acontecimientos almacenados de la consulta
                    $scope.listaAcontecimientosLocal = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        var aux = {};
                        aux = res.rows.item(i);
                        $scope.listaAcontecimientosLocal.push(aux);
                    }

                    //longitud y latitud del primero
                    var latitud_longitud = {
                        latitud: $scope.listaAcontecimientosLocal[0].Latitud,
                        longitud: $scope.listaAcontecimientosLocal[0].Longitud,
                        nombre: $scope.listaAcontecimientosLocal[0].Nombre
                    };

                    //lo asigno a variable global para pasarlo al mapa
                    $rootScope.latLong = latitud_longitud;
                    
                    //variable auxiliar por si quiero pasar más datos
                    var infoMapa = {
                        idAc: $scope.listaAcontecimientosLocal[0].Id,
                        nombre: $scope.listaAcontecimientosLocal[0].Nombre
                    };
                    
                    $rootScope.infMap = infoMapa;

                } else {
                    alert("Ningún resultado");
                    $scope.listaAcontecimientosLocal.push("Ningún resultado");
                }
            }, function (err) {
                console.error(err);
                alert(err);
            });

            //consulta para eventos
            var queryEventos = "SELECT * FROM Eventos WHERE Id_Acontecimiento=?";
            //ejecutar consulta
            $cordovaSQLite.execute(db, queryEventos, [$rootScope.IDAcontecimientoSeleccionado]).then(function (res) {
                if (res.rows.length > 0) {
                    //obtenemos lista de eventos del acontecimiento
                    $scope.listaEventosLocal = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        var aux = {};
                        aux = res.rows.item(i);
                        $scope.listaEventosLocal.push(aux);
                    }


                } else {
                    alert("Ningún resultado");
                    $scope.listaAcontecimientosLocal.push("Ningún resultado");
                }
            }, function (err) {
                console.error(err);
                alert(err);
            });
        });
/*
        $scope.setLatLong = function () {
            var latitud_longitud = {
                latitud: $scope.listaAcontecimientosLocal[0].Latitud,
                longitud: $scope.listaAcontecimientosLocal[0].Longitud
            };

            $rootScope.latLong = latitud_longitud;
        }; */
        //alert($rootScope.IDAcontecimientoSeleccionado);
    });